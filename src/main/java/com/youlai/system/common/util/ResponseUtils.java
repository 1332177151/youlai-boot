package com.youlai.system.common.util;

import cn.hutool.json.JSONUtil;
import com.youlai.system.common.result.Result;
import com.youlai.system.common.result.ResultCode;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

/**
 * 响应工具类
 *
 * @author Ray Hao
 * @since 2.0.0
 */
@Slf4j
public class ResponseUtils {

    /**
     * 异常消息返回(适用过滤器中处理异常响应)
     *
     * @param response  HttpServletResponse
     * @param resultCode 响应结果码
     */
    public static void writeErrMsg(HttpServletResponse response, ResultCode resultCode) {
        // 根据不同的结果码设置HTTP状态
        int status = HttpStatus.BAD_REQUEST.value();
        if (ResultCode.ACCESS_UNAUTHORIZED.getCode().equals(resultCode.getCode())){
            status = HttpStatus.UNAUTHORIZED.value();
        }
        if(ResultCode.TOKEN_INVALID.getCode().equals(resultCode.getCode())){
            status = HttpStatus.UNAUTHORIZED.value();
        }
        if(ResultCode.TOKEN_ACCESS_FORBIDDEN.getCode().equals(resultCode.getCode())){
            status = HttpStatus.FORBIDDEN.value();
        }


        response.setStatus(status);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setCharacterEncoding(StandardCharsets.UTF_8.name());

        try (PrintWriter writer = response.getWriter()) {
            String jsonResponse = JSONUtil.toJsonStr(Result.failed(resultCode));
            writer.print(jsonResponse);
            writer.flush(); // 确保将响应内容写入到输出流
        } catch (IOException e) {
            log.error("响应异常处理失败", e);
        }
    }

}
