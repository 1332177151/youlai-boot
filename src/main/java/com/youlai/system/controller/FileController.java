package com.youlai.system.controller;

import com.youlai.system.common.result.Result;
import com.youlai.system.model.dto.FileInfo;
import com.youlai.system.service.OssService;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Api(tags =  "07.文件接口")
@RestController
@RequestMapping("/api/v1/files")
@RequiredArgsConstructor
public class FileController {

    private final OssService ossService;

    @PostMapping
    @ApiOperation(value =  "文件上传")
    public Result<FileInfo> uploadFile(
            @RequestParam(value = "file") MultipartFile file
    ) {
        FileInfo fileInfo = ossService.uploadFile(file);
        return Result.success(fileInfo);
    }

    @DeleteMapping
    @ApiOperation(value =  "文件删除")
    @SneakyThrows
    public Result deleteFile(
            @RequestParam String filePath
    ) {
        boolean result = ossService.deleteFile(filePath);
        return Result.judge(result);
    }
}
