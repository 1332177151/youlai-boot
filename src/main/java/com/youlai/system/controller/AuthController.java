package com.youlai.system.controller;

import com.youlai.system.common.result.Result;
import com.youlai.system.model.dto.CaptchaResult;
import com.youlai.system.model.dto.LoginResult;
import com.youlai.system.service.AuthService;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Api(tags =  "01.认证中心")
@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
@Slf4j
public class AuthController {

    private final AuthService authService;

    @ApiOperation(value =  "登录")
    @PostMapping("/login")
    public Result<LoginResult> login(
            @RequestParam String username,
            @RequestParam String password
    ) {
        LoginResult loginResult = authService.login(username, password);
        return Result.success(loginResult);
    }

    @ApiOperation(value =  "注销")
    @DeleteMapping("/logout")
    public Result logout() {
        authService.logout();
        return Result.success();
    }

    @ApiOperation(value =  "获取验证码")
    @GetMapping("/captcha")
    public Result<CaptchaResult> getCaptcha() {
        CaptchaResult captcha = authService.getCaptcha();
        return Result.success(captcha);
    }

}
